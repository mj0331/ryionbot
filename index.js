var discord = require("discord.io");
var tmi = require('tmi.js');
var fs = require("fs");

var twitch_channels = ["mj0331"];

var ryionbot = new discord.Client({
    "token"     : fs.readFileSync("discord.token").toString(),
    "autorun"   : true 
});

var ryionbot_twitch = new tmi.client({
    identity:{
        username: "RyionBot",
        password: fs.readFileSync("twitch.token").toString()
    },
    channels: twitch_channels
});

var mainServer = { name: "The Hangarounds", mainChannel: "210467011992616961"};

//Some data

var serialisable = JSON.parse(fs.readFileSync("botdata.json").toString());

var superusers  = serialisable["superusers"];
var roleMap     = serialisable["roleMap"];
var data        = serialisable["data"];

//Help text map

var serviceIDMap = {
    all : 0,
    discord : 1,
    twitch : 2
}

var commandHelpMap = {
    "!help"     : "Displays all available commands",
    "!info"     : "Same as `!help`",
    "!ping"     : "Test command to check bot responsiveness",
    "!cointoss" : "Flips a coin. Result is either Heads or Tails",
    "!random"   : "Takes 2 parameters representing the interval ends from which the random number is picked",
    "!whois"    : "Takes 1 argument representing the name of the user to get info on.",
    "!iam"      : "Takes 1 argument representing the description you want to show up when you are !whois'd",
    "!remindme" : "Outputs a given string after a given number of minutes. Syntax: '!remindme <minutes> <text>",
    //"!teamlist" : "List all registered teams",
    //"!teammake" : "Create a new team (and disband the current one if you are it's founder)",
    "!setdata"  : "[Superuser] Sets variable values",
    "!givewip"  : "Transfers a set number of Worthless Internet Points™. Syntax: '!givewip <amount> <user>",
    "!checkwip" : "Checks how many Worthless Internet Points™ a player has. Syntax: '!checkwip <user>",
    "!resetwip" : "[Superuser] Resets the number of Worthless Internet Points™ of a given player. Syntax: '!resetwip <user>"
};

// The command-function map

var commandMap = {
    "!help"     : cmd_help,
    "!info"     : cmd_help,
    "!ping"     : cmd_pong,
    "!cointoss" : cmd_coin,
    "!random"   : cmd_randomnumber,
    "!whois"    : cmd_whois,
    "!iam"      : cmd_iam,
    "!remindme" : cmd_remind,
    //"!teamlist" : cmd_teamlist,
    //"!teammake" : cmd_teammake,
    "!bet"      : cmd_bet,
    "!cancelbet": cmd_cancelbet,
    "!togglebetlock": cmd_togglebetlock,
    "!betpayout": cmd_betpayout,
    //DEBUG ONLY
    "!resolvebets"  : cmd_resolvebets,
    "!setdata"  : cmd_setdata,
    "!givewip"  : cmd_giveWIP,
    "!checkwip" : cmd_checkWIP,
    "!resetwip" : cmd_resetWIP,
    "!animation": cmd_animation
};

//Command function implementations

function cmd_pong(user, userID, channelID, message, event, serviceID)
{
    bot_message(serviceID, channelID, "*sigh*...");
}

function cmd_help(user, userID, channelID, message, event, serviceID)
{
    bot_message(serviceID, channelID, "I may help you with some of the following: " + list_commands());
}

function cmd_coin(user, userID, channelID, message, event, serviceID)
{
    bot_message(serviceID, channelID, (random_int(1, 100) % 2 ? "Heads" : "Tails"));
}

function cmd_betpayout(user, userID, channelID, message, event, serviceID)
{
    if(!(user in data.bets))
    {
        bot_message(serviceID, channelID, user + " did not place a bet");
        return;
    }

    bot_message(serviceID, channelID, "Payout for " + user + " is " + getBetPayout(user));
}

function cmd_cancelbet(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    if(data.betsLocked)
    {
        bot_message(serviceID, channelID, "Bets are currently locked. You cannot cancel a bet right now.");
        return;
    }

    if(!data.bets[user])
    {
        bot_message(serviceID, channelID, "No bet on record for you, " + user);
        return;
    }

    WIP_transfer(null, user, data.bets[user].amount);
    data.teamtotals[data.bets[user].teamID] -= data.bets[user].amount;
    delete data.bets[user];

    bot_message(serviceID, channelID, user + "'s bet has been cancelled and the funds were transfered back(use `!checkwip " + user + "` to check you balance)");
}

function cmd_bet(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    args[1] = parseInt(args[1]);

    if(data.betsLocked)
    {
        bot_message(serviceID, channelID, "Bets are currently locked");
        return;
    }

    if(args.length != 3)
    {
        bot_message(serviceID, channelID, "Syntax is `!bet <amount> <teamID>`");
        return;
    }

    if(isNaN(args[1]))
    {
        bot_message(serviceID, channelID, "Amount parameter not a number");
        return;
    }

    if(args[1] <= 0)
    {
        bot_message(serviceID, channelID, "Amount cannot be negative");
        return;
    }

    var wipAvailable = WIP_check(user);
    if(wipAvailable == -1)
    {
        bot_message(serviceID, channelID, "You are not registered in the WIP system!");
        return;
    }

    if(args[1] >= data.points[user])
    {
        bot_message(serviceID, channelID, "You don't have enough points! (You wanted to bet " + args[1] + " points, but only have " + data.points[user] +" left)");
        return;
    }

    if(args[2] < 1 || args[2] > 4)
    {
        bot_message(serviceID, channelID, "Team ID not valid! (1 - Red, 2 - Blue, 3 - Green, 4 - Yellow)");
        return;
    }

    WIP_transfer(user, null, args[1]);

    data.bets[user] = {
        amount: parseInt(args[1]),
        teamID: args[2],
    };

    //Clear junk data if needed
    if(isNaN(data.teamtotals[args[2]]))
    {
        data.teamtotals[args[2]] = 0;
    }

    data.teamtotals[args[2]] += parseInt(args[1]);

    updateSerialisable();

    bot_message(serviceID, channelID,   user + " placed a bet of " + data.bets[user].amount + " on team #" + data.bets[user].teamID + "\n " +
                                        user + " will earn " + getBetPayout(user) + " if they win the bet");
}

function cmd_togglebetlock(user, userID, channelID, message, event, serviceID)
{
    data.betsLocked = !data.betsLocked;

    bot_message(serviceID, channelID, "Bets locked: " + data.betsLocked);
}

function cmd_resolvebets(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    if(!data.betsLocked)
    {
        bot_message(serviceID, channelID, "Cannot resolve bets if the bets are not locked yet");
        return;
    }

    if(args.length != 2)
    {
        bot_message(serviceID, channelID, "Cannot resolve bets! No winning side given!");
        return;
    }

    for(var key in data.bets)
    {
        if(data.bets[key] == args[1])
        {
            WIP_transfer(null, key, getBetPayout(key));
        }
    }

    clearAllBets();
    bot_message(serviceID, channelID, "Bets resolved! Use `!checkwip <username>` to see your new balance");
}

function cmd_teamlist(user, userID, channelID, message, event, serviceID)
{
    var teamlist = "Teams are:\n[TEAM NAME]\t\t[#1]\t\t[#2]\n";
    var teams = data.teams;

    for(var k in teams)
    {
        teamlist += teams[k].name + "\t\t" + teams[k].founder + "\t\t" + teams[k].member + "\n";
    }

    bot_message(serviceID, channelID, teamlist);
}

function cmd_teammake(user, userID, channelID, message, event, serviceID)
{
    var teams = data.teams;
    var args = getArgs(message);

    if(args.length <= 1)
    {
        bot_message(serviceID, channelID, "Syntax is `!teammake <teamname> [<teammate>]");
        return;
    }

    //TODO: finish team creation logic
}




function cmd_animation(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    if(args.length >= 2)
    {
        var anim_name = args[1];

        if(data.animations[anim_name] != null)
        {
            var frames = data.animations[anim_name].slice();
            var messageHandle;

            ryionbot.sendMessage({
                to      : channelID,
                message : "<Playing '" + anim_name + "' (" + frames.length + " frames)>"
            }, function(err, response){
                messageHandle = response.id;
            });

            function _sendFrames()
            {
                setTimeout(function()
                {
                    if(frames[0])
                    {
                        //bot_message(serviceID, channelID, frames.shift());
                        ryionbot.editMessage({
                            channelID   : channelID,
                            messageID   : messageHandle,
                            message     : frames.shift()
                        });
                        _sendFrames();
                    }
                }, data.animation_frametime);
            }
            
            _sendFrames();
        }
    }
}

function cmd_randomnumber(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    var a = parseInt(args[1], 10);
    var b = parseInt(args[2], 10);

    //Check if a and b are number
    if(isNaN(a) || isNaN(b))
    {
        bot_message(serviceID, channelID, "Those are not number enough for me!");
        return;
    }

    if(b < a)
    {
        var aux = a;
        a = b;
        b = aux;
    }

    bot_message(serviceID, channelID, random_int(a, b).toString());
}

function cmd_whois(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);
    if(args.length >= 2)
    {
        var target  = args[1];
    
        if(target in data.whoIs)
        {
            bot_message(serviceID, channelID, target + ": " +  data.whoIs[target]);
        }
        else
        {
            bot_message(serviceID, channelID, "Nothing on recond for " + target);
        }
    }
    else
    {
        bot_message(serviceID, channelID, "Wrong number of arguments for command. Type '!whois <username>' to get info on <username>");
    }
}

function cmd_iam(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);
    if(args.length >= 2)
    {
        data.whoIs[user] = args[1];
        updateSerialisable();
        bot_message(serviceID, channelID, user + " is now know for:\n" + data.whoIs[user]);
    }
    else
    {
        bot_message(serviceID, channelID, "Description empty!");
    }
}


function cmd_setdata(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    //Check for corrent number of args
    if(args.length == 3)
    {
        //Check if invoker is superuser
        if(isSuperuser(userID))
        {
            data[args[1]] = args[2];
            bot_message(serviceID, channelID, "set '" + args[1] + "' to '" + data[args[1]] + "'");
        }
        else
        {
            bot_message(serviceID, channelID, "Sorry, I'm afraid I cannot let you do that...");
        }
    }
    else
    {
        bot_message(serviceID, channelID, "That's now how you do that. Syntax is '!setdata <field> <value>'!");
    }
}

function cmd_remind(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    //Check for correct number of arguments
    if(args.length >= 3)
    {
        if(args[1] != NaN && parseInt(args[1]) >= 1)
        {
            args[1] = parseInt(args[1]);
            var msg = mergeIntoString(args, 2);

            var fnID = setTimeout(function(){bot_message(serviceID, channelID, msg);}, +args[1] * 60 * 1000);
            data["reminders"][fnID] = msg;

            bot_message(serviceID, channelID, "You shall be reminded in " + args[1] + " " + (args[1] > 1 ? "minutes!" : "minute!"));
        }
        else
        {
            bot_message(serviceID, channelID, "The value given for minutes is not a number and/or smaller or equal to 0!");
        }
    }
    else
    {
        bot_message(serviceID, channelID, "That's now how you do that. Syntax is '!remindme <time_in_seconds> <text>'!");
    }
}

function cmd_checkWIP(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    if(args.length == 2)
    {
        if(isValidMention(args[1]))
        {
            args[1] = nameFromID(idFromMention(args[1]));
        }

        if(args[1] in data["points"])
        {
            bot_message(serviceID, channelID, args[1] + " has " + data["points"][args[1]] + " Worthless Internet Points™");
        }
        else
        {
            bot_message(serviceID, channelID, "Found no entry for '" + args[1] + "'");
        }
    }
    else
    {
        bot_message(serviceID, channelID, "The function syntax is '!checkwip <user>");
    }
}

function cmd_resetWIP(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    //if(superusers.indexOf(user) != -1)
    if(isSuperuser(userID))
    {
        if(args[1])
        {
            if(isUser(args[1]))
            {
                WIP_reset(args[1]);
                cmd_checkWIP(user, userID, channelID, message, event, serviceID);
            }
            else
            {
                bot_message(serviceID, channelID, "User does not exist or is not registered in the WIP system");
            }
        }
        else
        {
            bot_message(serviceID, channelID, "No user specified");
        }
    }
    else
    {
        bot_message(serviceID, channelID, user + ", you are not super enough to do this!");
    }
}

function cmd_giveWIP(user, userID, channelID, message, event, serviceID)
{
    var args = getArgs(message);

    if(args.length != 3)
    {
        bot_message(serviceID, channelID, "Usage is !givewip <name> <amount>");
        return;
    }

    if(isValidMention(args[1]))
    {
        args[1] = nameFromID(idFromMention(args[1]));
    }

    if(!isUser(user) || !isUser(args[1]))
    {
        bot_message(serviceID, channelID, "Either the sender or the recipient are either not a real user nor a real dev");
        return;
    }

    if(WIP_transfer(user,args[1], parseInt(args[2])) == -1)
    {
        bot_message(serviceID, channelID, "The transfer could not be done due to a bad amount(either more than you have or less than 0)");
        return;
    }

    bot_message(serviceID, channelID, "Transfer complete. " + user + " now has " + data["points"][user] + " WIP and " + args[1] + " has " + data["points"][args[1]] + " WIP");
    updateSerialisable();
}

//Worthless Internet Points™ system functions

function WIP_transfer(u_from, u_to, amount)
{
    if(!u_from in data["points"])
    {
        u_from = "RyionBot";
    }

    if(!(u_to in data["points"]))
    {
        u_to = "RyionBot";
    }

    if(data["points"][u_from] < amount || amount <= 0)
    {
        return -1;
    }

    data["points"][u_from] -= +amount;
    data["points"][u_to] += +amount;

    updateSerialisable();

    return 0;
}

function WIP_check(u)
{
    if(u in data["points"])
    {
        return data["points"][u];
    }
    return -1;
}

function WIP_reset(u)
{
    data["points"][u] = parseInt(data["defaultPoints"]);
    updateSerialisable();
}

//Utility functions

function getBetUserPerc(name)
{
    return data.bets[name].amount / data.teamtotals[data.bets[name].teamID];
}

function getBetPayout(name)
{
    var s = 0;
    for(var key in data.teamtotals)
    {
        s += data.teamtotals[key];
    }

    return s * getBetUserPerc(name);
}

function clearAllBets()
{
    data.bets = {};
    data.teamtotals = {};

    updateSerialisable();
}

function isValidMention(mention)
{
    if(idFromMention(mention) == -1)
    {
        return false;
    }
    return isUser(nameFromID(idFromMention(mention)));
}

function idFromMention(mention)
{
    if(!mention || mention.indexOf("<@") == -1)
    {
        return -1;
    }

    return mention.substring(2, mention.length-1);
}

function getAnimationState(animName, stateID)
{
    if(stateID >= data.animations[animName].length)
    {
        return null;
    }
    else
    {
        return data.animations[animName][stateID];
    }
}

function getArgs(message)
{
    //Split the input by spaces
    var word_array = message.split(" ");
    var args_array = [];
    var temp_arg = "";

    for(var key = 0; key < word_array.length; key++)
    {
        //If an element start with quotes, it's the start of a string'
        if(word_array[key][0] == "\"" || word_array[key][0] == "\'")
        {
            var found_end = false;

            //Search for the end of the string and also inceremnt the key counter
            for(var j = key; j < word_array.length && !found_end; j++, key++)
            {
                //Add the word to the argument
                temp_arg += word_array[j] + " ";

                //If there's another quote at the end, we've reached the end of the argument
                if(word_array[j][word_array[j].length - 1] == "\"" || word_array[j][word_array[j].length - 1] == "\'")
                {
                    found_end = true;
                }
            }
            //Add the final argument to the argument array
            args_array.push(temp_arg.substring(1, temp_arg.length-2));
        }
        else
        {
            args_array.push(word_array[key]);
        }
    }

    return args_array;
}

function mergeIntoString(arr, start)
{
    var str = "";

    for(var i = start; i < arr.length; i++)
    {
        str += arr[i] + " ";
    }

    return str;
}

function random_int(min_val, max_val)
{
    return Math.round(Math.random() * (max_val - min_val) + min_val);
}

function list_commands()
{
    var commands = "\n";

    for(var key in commandMap)
    {
        commands += "`" + key  + "`" + "\t - " + commandHelpMap[key] + "\n";
    }

    return commands + "";
}

function getServerByName(serverlist, name)
{
    for(var key in serverlist)
    {
        if(serverlist[key].name == name)
        {
            return serverlist[key];
        }
    }
    return null;
}

function userFromID(id)
{
    var server = getServerByName(ryionbot.servers, mainServer.name);

    for(var k in server.members)
    {
        if(k == id)
        {
            return server.members[k];
        }
    }
    return null;
}

function userFromName(name)
{
    var server = getServerByName(ryionbot.servers, mainServer.name);

    for(var k in server.members)
    {
        if(server.members[k].username == name)
        {
            return server.members[k];
        }
    }
    return null;
}

function nameFromID(id)
{
    var server = getServerByName(ryionbot.servers, mainServer.name);

    for(var k in server.members)
    {
        if(k == id)
        {
            return server.members[k].username;
        }
    }
    return null;
}

function idFromName(name)
{
    var server = getServerByName(ryionbot.servers, mainServer.name);

    for(var k in server.members)
    {
        if(k == id)
        {
            return server.members[k].username;
        }
    }
    return null;
}

function isUser(name)
{
    var server = getServerByName(ryionbot.servers, mainServer.name);

    for(var k in server.members)
    {
        if(server.members[k].username == name)
        {
            return true;
        }
    }
    return false;
}

function isSuperuser(userID)
{
    var server = getServerByName(ryionbot.servers, mainServer.name);

    if(server != null)
    {
        var user = server.members[userID];
        if(user != null || user != undefined)
        {
            for(var k in user.roles)
            {
                if(user.roles[k] == roleMap.superuser)
                {
                    return true;
                }
            }
        }
    }
    return false;
}


function updateSerialisable()
{
    serialisable["superusers"] = superusers;
    serialisable["roleMap"] = roleMap;
    serialisable["data"] = data;

    fs.writeFileSync("botdata.json", JSON.stringify(serialisable, null, 2));
}

function parseMessage(user, userID, channelID, m, event, serviceID)
{
    if(serviceID == serviceIDMap.twitch)
    {
        //Trim the leading # in the channel name if this is from Twitch
        channelID = channelID.substring(1);
    }

    //Trim whitespace
    m = m.trim();

    //Check if the message is a command
    if(m[0] == "!")
    {
        //Check if we have a function bound to that command
        
        var cmd = m.split(" ")[0];
        if(cmd in commandMap)
        {
            //If we do, run the bound command
            commandMap[cmd](user, userID, channelID, m, event, serviceID);
        }
        else
        {
            //If we don't, let the user know
            cmd_help(user, userID, channelID, m, event, serviceID);
        }
    }
    //If this is a normal message, check if sender is muted
    else
    {
        if(data["isMuteEnabled"] == "true")
        {
            if(data["muted"].indexOf(user) != -1)
            {
                if(serviceID == serviceIDMap.discord){
                    ryionbot.deleteMessage(channelID, m.id, function(err){
                        if(err)
                        {
                            console.log(err);
                        }
                    });
                }
            }
        }
    }
}

function bot_message(serviceID, channelID, message)
{
    if(serviceID == 1){
        //Discord message
        ryionbot.sendMessage({
            to      : channelID,
            message : message
        }, function(err, response){
            if(err){
                console.log(err);
            }
        });
    }else if(serviceID == 2){
        //'tis a Twitch message
        ryionbot_twitch.say(channelID, message);
        console.log("RyionBot [Twitch]-> " + message);
    }else{
        //Discord message
        ryionbot.sendMessage({
            to      : channelID,
            message : message
        }, function(err, response){
            if(err){
                console.log(err);
            }
        });

        //'tis a Twitch message
        ryionbot_twitch.say(channelID, message);
        console.log("RyionBot [Twitch]-> " + message);
    }
}

ryionbot.on("ready", function(event){
    console.log(ryionbot.username + " ready");
    bot_message(true, mainServer.mainChannel, "RyionBot Discord now online");
});

//Set up command listener
ryionbot.on('message', function(user, userID, channelID, message, event) {
    console.log(user + " [Discord]-> " + message);
    parseMessage(user, userID, channelID, message, event, serviceIDMap.discord);
});

ryionbot.on("any", function(event) {
	//console.log(event);
});


ryionbot_twitch.on("connected", function(address, port){
    ryionbot_twitch.say(twitch_channels[0], "RyionBot Twitch now online");
});

ryionbot_twitch.on("chat", function (channel, userstate, message, self){
    //Ignore own messages
    if(self) return;

    console.log(userstate['display-name'] + " [Twitch]-> " + message);
    parseMessage(userstate['display-name'], null, channel, message, userstate, serviceIDMap.twitch);
});

ryionbot_twitch.connect();
